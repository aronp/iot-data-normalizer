<?php 

  function fetch_mysql(){
    
    $conn = mysqli_init();
    if (!$conn) {
      die("mysqli_init failed");
    }

    $conn->options(MYSQLI_OPT_SSL_VERIFY_SERVER_CERT, true);
    $conn->ssl_set(NULL, NULL,"/usr/local/etc/openssl/cert.pem",NULL,NULL); 

    if ($conn->connect_errno) {
      echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }

    $db_config = parse_ini_file(".env");
    $db_user = $db_config["DB_USER"];
    $db_name = $db_config["DB_NAME"];
    $db_host = $db_config["DB_HOST"];
    $db_password = $db_config["DB_PASS"];

    $conn->real_connect($db_host, $db_user, $db_password, $db_name);
    // echo $conn->host_info . "\n"; 
    return $conn;
    
  }