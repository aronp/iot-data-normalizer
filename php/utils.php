<?php 

  function update_users_query($args) {
    $query = "UPDATE users SET companyId = ".$args["company_id"]." where email LIKE";
    $masks = $args["masks"];
    $i = 0; 
    if (count($masks) > 0) {
      foreach($masks as $m) {
        if ($i > 0) {
          $query = $query.' OR email LIKE "'.$m.'"';
        } else {
          $query = $query.' "'.$m.'"';
        }
        $i = $i + 1; 
      }
    }
    return $query; 
  }

  function select_affected_users_query($args) {
    $query = "SELECT * FROM users WHERE email LIKE";
    $masks = $args["masks"];
    $i = 0; 
    if (count($masks) > 0) {
      foreach($masks as $m) {
        if ($i > 0) {
          $query = $query.' OR email LIKE "'.$m.'"';
        } else {
          $query = $query.' "'.$m.'"';
        }
        $i = $i + 1; 
      }
    }
    $query = $query." AND companyId != ".$args["company_id"]; 
    return $query; 
  }

  function backup_affected_users($user_args) {
    $backup_data = json_decode(file_get_contents("backups/data.json"), true); 
 
    $user_records = []; 
    foreach($user_args as $arg) { 
      $user["user_id"] = $arg["id"];
      $user["previous_company_id"] = $arg["companyId"]; 
      array_push($user_records, $user); 
    }

    $date_obj["modified_date"] = time();
    $date_obj["user_records"] = $user_records;

    array_push($backup_data, $date_obj);  
    file_put_contents("backups/data.json", json_encode($backup_data, JSON_PRETTY_PRINT));
  }

  function undo_previous_backup() {
    $backup_data = json_decode(file_get_contents("backups/data.json"), true); 
    if (count($backup_data) > 0) {
      array_pop($backup_data);
    } 
    file_put_contents("backups/data.json", json_encode($backup_data, JSON_PRETTY_PRINT));
  }
