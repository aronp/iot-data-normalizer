<?php

  require_once "php/utils.php"; 
  require_once "php/connection.php"; 

  $mysqli = fetch_mysql();
  $company_ids = json_decode(file_get_contents("company_data.json"), true); 

  foreach($company_ids as $data) {
    $affected_users = $mysqli->query(select_affected_users_query($data));
    if ($affected_users->num_rows > 0) {
      backup_affected_users($affected_users);  
      $then = time();
      $update_users_query = update_users_query($data); 
      $query_result = $mysqli->query($update_users_query);
      if ($query_result) {
        echo "\n"."company name: ".$data["company_name"]."\n";
        echo "# of users updated: ".$affected_users->num_rows."\n"; 
        echo "completed in ". (time() - $then). " seconds"."\n";
      } else {
        undo_previous_backup(); 
        echo "Error. \n\n";
        print_r($mysqli);
        print_r($update_users_query);
      }
    } else {
      echo "\n"."company name: ".$data["company_name"]."\n";
      echo "No users updated"."\n"; 
    }
  }
  